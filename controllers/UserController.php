<?php
require('config/autoloader.php');
class UserController
{
    private static $pdo;
    private static $repository;
    public function __construct($pdo)
    {
        self::$pdo = $pdo;
        self::$repository = new UserRepository(self::$pdo);
    }
    static function registerNewUser($postData, $file)
    {
        setcookie('registerFormData', json_encode($postData), time() + 5);
        if (empty($postData)) return null;
        // var_dump($postData, $file);
        $name = addslashes($postData['name']);
        $email = addslashes($postData['email']);
        $age = addslashes($postData['age']);
        $username = addslashes($postData['username']);
        $password = base64_encode(addslashes($postData['password']));
        if ($file['type'] === 'image/gif' ||
            $file['type'] === 'image/png' ||
            $file['type'] === 'image/jpeg' ||
            $file['type'] === 'image/bmp' ||
            $file['type'] === 'image/webp') {
            try {
                $avatarName = explode('.', $file['name']);
                $avatarName = $username . '.' . $avatarName[count($avatarName) - 1];
                $user = new User($username, $email, $name, $password, $age, $avatarName);
                if (!file_exists('uploads'))
                    mkdir('uploads');
                if (!file_exists('uploads' . DIRECTORY_SEPARATOR . 'avatars'))
                    mkdir('uploads' . DIRECTORY_SEPARATOR . 'avatars');
                if (self::$repository->registerNewUser($user)
                    &&
                    move_uploaded_file(
                    $file['tmp_name'],
                    'uploads' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR . $avatarName
                )) {
                    setcookie('registerErrors', null);
                    setcookie('registerFormData', null);
                    return;
                }
                return false;
            } catch (Exception $err) {
                setcookie('registerErrors', json_encode([
                    'erro' => 'Ocorreu um Erro',
                    'stack' => $err->getMessage()
                ]), time() + 5);
            }
        } else return;
    }
}