<?php

class Database
{
    private $conn;
    public function __construct()
    {
        $dsn = 'mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME;
        try {
            $this->conn = new PDO($dsn, DB_USER, DB_PASS, DB_OPTS);
        } catch (PDOException $err) {
            echo ("ERR: " . $err->getMessage());
        }
    }
    public function getConn()
    {
        return $this->conn;
    }
}
