<?php

class User
{
    private $id;
    private $username;
    private $email;
    private $name;
    private $age;
    private $password;
    private $createdAt;
    private $avatar;

    public function __construct($username, $email, $name, $password, $age, $avatar, $id = null)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->name = $name;
        $this->password = $password;
        $this->createdAt = date("Y-m-d H:i:s");
        $this->age = $age;
        $this->avatar = $avatar;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getUsername()
    {
        return $this->username;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    public function getAge()
    {
        return $this->age;
    }
    public function getAvatar()
    {
        return $this->avatar;
    }
    // 
    public function setUsername($username)
    {
        $this->username = $username;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setAge($age)
    {
        $this->age = $age;
    }
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    public function setPassword($password)
    {
        $password = base64_encode(addslashes($password));
        $this->password = $password;
    }
}