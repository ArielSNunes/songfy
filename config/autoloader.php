<?php

spl_autoload_register(function ($class) {
    $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    $folders = [
        'models',
        'controllers',
        'repositories',
        'views'
    ];
    foreach ($folders as $folder) {
        $link = $folder . DIRECTORY_SEPARATOR . $class . '.php';
        try {
            if (file_exists($link)) require($link);
        } catch (Exception $err) {
            echo ($err->getMessage());
        }
    }
});