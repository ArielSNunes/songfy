<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
require('autoloader.php');

define('APP_NAME', 'Songfy');
define('BASE_URL', 'http://localhost:3000/pessoais/songfy');

define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3306');
define('DB_USER', 'ArielSNunes');
define('DB_PASS', 'Ariel');
define('DB_NAME', 'songfy');
define('DB_OPTS', [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => true,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
]);