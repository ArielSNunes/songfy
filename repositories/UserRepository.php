<?php

class UserRepository
{
    private $conn;
    public function __construct($pdo)
    {
        $this->conn = $pdo;
    }
    public function selectAll()
    {
        $sql = "SELECT * FROM users";
        $sql = $this->conn->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetchAll();
        return null;
    }
    public function selectByEmail($email)
    {
        if (!isset($email) || empty($email))
            return false;
        $email = addslashes($email);
        if (!is_string($email))
            return false;
        $sql = "SELECT * FROM users WHERE email = :email";
        $sql = $this->conn->prepare($sql);
        $sql->bindValue('email', $email);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetch();
        return null;
    }
    public function selectByUsername($username)
    {
        if (!isset($username) || empty($username))
            return false;
        $username = addslashes($username);
        if (!is_string($username))
            return false;
        $sql = "SELECT * FROM users WHERE username = :username";
        $sql = $this->conn->prepare($sql);
        $sql->bindValue('username', $username);
        $sql->execute();
        if ($sql->rowCount() > 0)
            return $sql->fetch();
        return null;
    }
    public function registerNewUser($user)
    {
        $sql = "INSERT INTO users (username, name, email, password, created_at, avatar_image, age) VALUES (:username, :name, :email, :password, :createdAt, :avatar, :age)";
        $sql = $this->conn->prepare($sql);
        $sql->bindValue('username', $user->getUsername());
        $sql->bindValue('name', $user->getName());
        $sql->bindValue('email', $user->getEmail());
        $sql->bindValue('password', $user->getPassword());
        $sql->bindValue('createdAt', $user->getCreatedAt());
        $sql->bindValue('avatar', $user->getAvatar());
        $sql->bindValue('age', $user->getAge());
        $sql->execute();
        if ($this->conn->lastInsertId() > 0)
            return true;
        return false;
    }
}
