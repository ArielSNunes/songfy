; (function (win, $) {
    var $fileInput = $('input[type=file]#file');
    var $toBecomeFileName = $('#selectedFile p');
    $fileInput.on('change', function (e) {
        var file = $fileInput
            .val().replace("C:\\fakepath\\", "");
        $toBecomeFileName.text(file.toString());
    });
})(window, jQuery);