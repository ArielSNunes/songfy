<?php
require('config/globalConfig.php');

$url = isset($_GET['url']) ? $_GET['url'] : 'home';
$url = is_array($url) ? $url[0] : $url;

if (isset($url)) {
    $file = 'views' . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . $url . '.php';
    if (file_exists($file))
        include($file);
    else
        include('views' . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . '404.php');
}