<?php
function verifyActive($url, $linkName)
{
	if ($url === $linkName)
		echo ('active');
}
if (isset($_GET['url']))
	$url = strpos($_GET['url'], DIRECTORY_SEPARATOR)
	? substr($_GET['url'], 0, strpos($_GET['url'], DIRECTORY_SEPARATOR))
	: $_GET['url'];
?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge" />
		<title><?= APP_NAME ?></title>
		<link rel="stylesheet" href="<?= BASE_URL ?>/public/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?= BASE_URL ?>/public/css/styles.css" />
	</head>

	<body>
		<nav class="navbar navbar-expand-lg fixed-bottom navbar-dark bg-primary <?php if ($url === 'home') echo ('d-none'); ?>">
			<a class="navbar-brand" href="<?= BASE_URL . '/home' ?>">
				<?= APP_NAME ?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
			aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item">
						<a class="nav-link <?= verifyActive($url, 'home') ?>"
							href="<?= BASE_URL . '/home' ?>">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?= verifyActive($url, 'albuns') ?>"
							href="<?= BASE_URL . '/albuns' ?>">Albums</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?= verifyActive($url, 'songs') ?>"
							href="<?= BASE_URL . '/songs' ?>">Songs</a>
					</li>

				</ul>
				<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
					<li class="nav-item">
						<a class="nav-link <?= verifyActive($url, 'register') ?>"
							href="<?= BASE_URL . '/register' ?>">Register</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?= verifyActive($url, 'login') ?>"
							href="<?= BASE_URL . '/login' ?>">Login</a>
					</li>
				</ul>
			</div>
		</nav>