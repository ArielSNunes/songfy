<?php
include('views/partials/header.php');

if (isset($_GET['url']))
    $url = strpos($_GET['url'], DIRECTORY_SEPARATOR)
    ? substr($_GET['url'], 0, strpos($_GET['url'], DIRECTORY_SEPARATOR))
    : $_GET['url'];
if ($_GET['url'] !== $url) header('Location: ' . BASE_URL . DIRECTORY_SEPARATOR . $url);

require('config/autoloader.php');
$db = new Database();
$Controller = new UserController($db->getConn());

if (isset($_POST['submit']) && !empty($_FILES['file']))
    $Controller::registerNewUser($_POST, $_FILES['file']);
?>

<div class="container mt-5">
    <?php
    if (isset($_COOKIE['registerErrors'])) {
        $cookieData = json_decode($_COOKIE['registerErrors']);
        echo ("<div class=\"alert alert-danger\" role=\"alert\">" . $cookieData->stack . "</div>");
    }
    if (isset($_COOKIE['registerFormData'])) {
        $cookieData = json_decode($_COOKIE['registerFormData']);
        $name = $cookieData->name;
        $age = $cookieData->age;
        $username = $cookieData->username;
        $email = $cookieData->email;
    }
    ?>
    <form method="POST" enctype="multipart/form-data">
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="name" 
                        aria-describedby="nameHelp" 
                        placeholder="Enter Name"
                        name="name"
                        value="<?php if (isset($name)) echo ($name) ?>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="age">Age</label>
                    <input 
                        type="number"
                        min="0" max="99" 
                        class="form-control" 
                        id="age" 
                        aria-describedby="ageHelp" 
                        name="age"
                        value="<?php 
                                if (isset($age)) echo ($age);
                                else echo (13);
                                ?>"/>
                    <span 
                        id="usernameHelp" 
                        class="form-text text-muted">
                        Recomended for 
                        <span class="badge badge-pill badge-danger">+13</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input 
                        type="email" 
                        class="form-control" 
                        id="email" 
                        aria-describedby="emailHelp" 
                        placeholder="Enter email"
                        name="email"
                        value="<?php if (isset($email)) echo ($email) ?>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="username" 
                        aria-describedby="usernameHelp" 
                        placeholder="Enter username"
                        name="username"
                        value="<?php if (isset($username)) echo ($username) ?>"/>
                    <small 
                        id="usernameHelp" 
                        class="form-text text-muted">
                        You'll use the Username to Login.
                    </small>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input 
                        type="password" 
                        class="form-control" 
                        id="password" 
                        placeholder="Password"
                        name="password"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="confirmPassword">Confirm Password</label>
                    <input 
                        type="password" 
                        class="form-control" 
                        id="confirmPassword" 
                        placeholder="Password"
                        name="confirmPassword"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group file-handler">
                    <label for="file" class="btn bg-primary text-light btn-block my-2">Pick a Avatar</label>
                    <input 
                    type="file" class="form-control-file" id="file" name="file" />
                    <div id="selectedFile" class="selectedFile">
                        <p class="text-center"></p>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
    </form>
</div>
</div>
<?php include('views/partials/footer.php'); ?>