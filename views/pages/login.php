<?php
include('views/partials/header.php');

if (isset($_GET['url']))
    $url = strpos($_GET['url'], DIRECTORY_SEPARATOR)
    ? substr($_GET['url'], 0, strpos($_GET['url'], DIRECTORY_SEPARATOR))
    : $_GET['url'];
if ($_GET['url'] !== $url) header('Location: ' . BASE_URL . DIRECTORY_SEPARATOR . $url);

require('classes/Database.php');
require('models/UserModel.php');
$db = new Database();
$user = new UserModel($db->getConn());
if (isset($_POST['submit']))
    var_dump($_POST);
?>

<div class="container mt-5">
    <form method="POST">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input 
                        type="email" 
                        class="form-control" 
                        id="email" 
                        aria-describedby="emailHelp" 
                        placeholder="Enter email"
                        name="email"/>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input 
                        type="password" 
                        class="form-control" 
                        id="password" 
                        placeholder="Password"
                        name="password"/>
                </div>
            </div>
        </div>
        <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
    </form>
</div>

<?php include('views/partials/footer.php'); ?>