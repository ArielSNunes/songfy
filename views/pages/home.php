<?php include('views/partials/header.php'); ?>

<div style="background-image: url('<?= BASE_URL ?>/public/images/home-bg.jpeg');" 
    class="imaged-container d-flex flex-column justify-content-center align-items-center pb-2 text-light text-sm-center ">
    <h1 class="display-2 text-sm-center px-5">Welcome to <span class="text-primary">Songfy</span></h1>
    <p class="lead">
        <span class="text-primary">Share your feelings </span> through music
        <div class="mt-3">
            <a class="btn btn-light"
                href="<?= BASE_URL . '/register' ?>" role="button">
                Login
            </a>
            <a class="btn btn-primary"
                href="<?= BASE_URL . '/register' ?>" role="button">
                Register
            </a>
        </div>
    </p>
</div>

<?php include('views/partials/footer.php'); ?>