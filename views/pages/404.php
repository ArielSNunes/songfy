<?php include('views/partials/header.php');

if (isset($_GET['url']))
    $url = strpos($_GET['url'], DIRECTORY_SEPARATOR)
    ? substr($_GET['url'], 0, strpos($_GET['url'], DIRECTORY_SEPARATOR))
    : $_GET['url'];
if ($_GET['url'] !== $url) header('Location: ' . BASE_URL . DIRECTORY_SEPARATOR . $url);
?>

<div class="container mt-3">
    <h3 class="text-center">404 - Page Not Available</h3>
</div>

<?php include('views/partials/footer.php'); ?>